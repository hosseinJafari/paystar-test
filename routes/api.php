<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1'], function () {
    Route::get('pays', 'PaymentsController@transactionList')->name('pays.index');
    Route::get('userInfo', 'PaymentsController@getUserInfo')->name('pays.userInfo');
    Route::post('transferTo', 'PaymentsController@transferTo')->name('pays.transferTo');
});

