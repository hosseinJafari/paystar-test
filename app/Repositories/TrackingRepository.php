<?php

namespace App\Repositories;

use App\Models\Tracking;
use App\Constanses\TrackingStatus;
use App\Repositories\Interfaces\ITrackingRepository;

class TrackingRepository implements ITrackingRepository
{
    public function store(int $userId, int $trackingTypeId)
    {
        // TODO: Implement store() method.
        return Tracking::create([
            'trackingTypeId' => $trackingTypeId,
            'userId' => $userId,
            'status' => TrackingStatus::$FAIL
        ]);
    }

    public function changeStatus(int $id, string $status)
    {
        // TODO: Implement changeStatus() method.
        $tracking = Tracking::find($id);
        $tracking->status = $status;
        $tracking->save();
        return $tracking;
    }
}
