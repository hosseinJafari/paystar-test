<?php


namespace App\Repositories\Interfaces;


interface ITransferTransactionsRepository
{
    public function store($data);
}
