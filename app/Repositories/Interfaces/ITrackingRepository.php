<?php

namespace App\Repositories\Interfaces;

use App\Constanses\TrackingStatus;

interface ITrackingRepository
{
    public function store(int $userId, int $trackingTypeId);

    public function changeStatus(int $id, string $status);
}
