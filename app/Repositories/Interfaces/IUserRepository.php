<?php

namespace App\Repositories\Interfaces;

use App\Http\Requests\UserRequest;

interface IUserRepository
{
    public function index();

    public function store($customerInfo);

    public function edit(int $id);

    public function update($customerInfo, int $id);

    public function destroy(int $id);

    public function getUser($mobile);
}
