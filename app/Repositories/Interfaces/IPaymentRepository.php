<?php


namespace App\Repositories\Interfaces;


interface IPaymentRepository
{
    public function pays(int $trackId, int $offset, int $length);

    public function userInformation(int $trackId, string $nationalId);

    public function transferTo();
}
