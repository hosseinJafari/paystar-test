<?php

namespace App\Repositories\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Interfaces\IUserRepository',
            'App\Repositories\UserRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\ITrackingRepository',
            'App\Repositories\TrackingRepository'
        );
        $this->app->bind(
            'App\Repositories\Interfaces\ITransferTransactionsRepository',
            'App\Repositories\TransferTransactionsRepository'
        );
    }
}
