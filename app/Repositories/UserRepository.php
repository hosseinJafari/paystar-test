<?php

namespace App\Repositories;

use App\Models\User;
use App\Http\Requests\UserRequest;
use App\Repositories\Interfaces\IUserRepository;

class UserRepository implements IUserRepository
{
    public function index()
    {
        return User::paginate(25);
    }

    public function store($customerInfo)
    {
        return User::create([

            'name' => $request->name,
        ]);
    }

    public function edit(int $id)
    {
        return User::find($id);
    }

    public function update($customerInfo, int $id)
    {
        $user = $this->edit($id);
        $user->firstName = $request['firstName'];
        $user->lastName = $request['lastName'];
        $user->birthdate = $request['birthdate'];
        $user->mobile = $request['mobile'];
        $user->customerTypeCode = $request['customerTypeCode'];
        $user->customerDescription = $request['customerDescription'];
        $user->save();
        return $user;
    }

    public function destroy(int $id)
    {
        return User::destroy($id);
    }

    public function getUser($mobile)
    {
        return User::where('mobile', $mobile)->first();
    }
}
