<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackingType extends Model
{
    use HasFactory;

    protected $table = 'trackings_types';
    protected $fillable = ['title'];
}
