<?php

namespace App\Classes\Providers;

use App\Classes\Finnotech\IFinnotechService;
use Illuminate\Support\ServiceProvider;

class FinnotechServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'App\Classes\Finnotech\IFinnotechService',
            'App\Classes\Finnotech\FinnotechService'
        );
    }
}
