<?php


namespace App\Classes\Models;


class CustomerInfo
{
    public $cif;
    public $nid;
    public $firstname;
    public $lastname;
    public $birthdate;
    public $mobile;
    public $customerSubtypeCode;
    public $customerSubtypeDescription;
}
