<?php


namespace App\Classes\Models;


class TransferToInfos
{
    public $amount;
    public $description;
    public $destinationFirstname;
    public $destinationLastname;
    public $destinationNumber;
    public $paymentNumber;
    public $deposit;
    public $sourceFirstName;
    public $sourceLastName;
    public $reasonDescription;
}
