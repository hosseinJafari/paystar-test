<?php

namespace App\Classes\Models;

class FinnotechResult
{
    public $ok;
    public $status;
    public $result;

    public function __construct(bool $ok, int $status, $result)
    {
        $this->ok = $ok;
        $this->status = $status;
        $this->result = $result;
    }
}
