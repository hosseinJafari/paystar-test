<?php


namespace App\Classes\Finnotech;


use App\Classes\Models\TransferToInfos;

interface IFinnotechService
{
    public function pays(string $deposit, int $trackId, string $fromDate, string $toDate, int $offset = 0, int $length = 20);

    public function customerInfo(string $nationalId, int $trackId);

    public function transferTo(TransferToInfos $data, int $trackId);

}
