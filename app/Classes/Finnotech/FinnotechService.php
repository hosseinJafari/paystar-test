<?php

namespace App\Classes\Finnotech;

use App\Classes\Models\CustomerInfo;
use App\Classes\Models\FinnotechResult;
use App\Classes\Models\TransferToInfos;

class FinnotechService implements IFinnotechService
{
    private $baseUrl = 'https://apibeta.finnotech.ir/';
    private $token = 'ASFsa45ASFSAFSAff5f4sa8e4few8fwefew987f54dsf8ew7f4eswf7wef78e8weewe89sdadsadsafd56aWEFRwefefwerew9rew';
    private $clientId = 'C1uE7FHqO9SHf87TM5ExtNWH7H1ZKB80emDz8c5f';

    public function pays(string $deposit, int $trackId, string $fromDate, string $toDate, int $offset = 0, int $length = 20)
    {
        // TODO: Implement pays() method.
        $url = $this->baseUrl . "/oak/v2/clients/{$this->clientId}/deposits/{$deposit}/payas?trackId={$trackId}&fromDate={$fromDate}&toDate={$toDate}&offset={$offset}&length={$length}";
        $response = Http::withHeaders(
            ['content-type' => 'application/json', 'authorization' => "Bearer {$this->token}"]
        )->get($url);
        if ($response->status() == 200) {
            $data = $response->json()['result'];
            $result = new FinnotechResult(true, 200, $data);
            return $result;
        }

        $result = new FinnotechResult(false, $response->status(), null);
        return $result;
    }

    public function customerInfo(string $nationalId, int $trackId)
    {
        $url = $this->baseUrl . "/oak/v2/clients/{$this->clientId}/users/{$nationalId}/customerInfo?trackId={$trackId}";
        $response = Http::withHeaders(
            ['content-type' => 'application/json', 'authorization' => "Bearer {$this->token}"]
        )->get($url);

        if ($response->status() == 200) {
            $data = $this->mapDataToCustomerInfo($response->json()['result']);
            $result = new FinnotechResult(true, 200, $data);
            return $result;
        }

        $result = new FinnotechResult(false, $response->status(), null);
        return $result;
    }

    private function mapDataToCustomerInfo($data): CustomerInfo
    {
        $customerInfo = new CustomerInfo();
        $customerInfo->cif = $data['cif'];
        $customerInfo->nid = $data['nid'];
        $customerInfo->firstname = $data['firstname'];
        $customerInfo->lastname = $data['lastname'];
        $customerInfo->birthdate = $data['birthdate'];
        $customerInfo->mobile = $data['mobile'];
        $customerInfo->customerSubtypeCode = $data['customerSubtypeCode'];
        $customerInfo->customerSubtypeDescription = $data['customerSubtypeDescription'];
        return $customerInfo;
    }

    public function transferTo(TransferToInfos $data, int $trackId)
    {
        // TODO: Implement transferTo() method.

        $url = $this->baseUrl . "/oak/v2/clients/{$this->clientId}/transferTo?trackId={$trackId}";
        $response = Http::withHeaders(
            ['content-type' => 'application/json', 'authorization' => "Bearer {$this->token}"]
        )->post($url, [
            "amount" => $data->amount,
            "description" => $data->description,
            "destinationFirstname" => $data->destinationFirstname,
            "destinationLastname" => $data->destinationLastname,
            "destinationNumber" => $data->destinationNumber,
            "paymentNumber" => $data->paymentNumber,
            "deposit" => $data->deposit,
            "sourceFirstName" => $data->sourceFirstName,
            "sourceLastName" => $data->sourceLastName,
            "reasonDescription" => $data->reasonDescription
        ]);

        if ($response->status() == 200) {
            $data = $this->mapDataToCustomerInfo($response->json()['result']);
            $result = new FinnotechResult(true, 200, $data);
            return $result;
        }

        $result = new FinnotechResult(false, $response->status(), null);
        return $result;
    }
}
