<?php

namespace App\Http\Controllers\Api;

use App\Classes\Finnotech\IFinnotechService;
use App\Classes\Models\TransferToInfos;
use App\Constanses\TrackingTypes;
use App\Repositories\Interfaces\IUserRepository;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Constanses\TrackingStatus;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use App\Helpers\Constanse\ResponseStatus;
use App\Helpers\Constanse\ResponseMessage;

use App\Repositories\Interfaces\ITrackingRepository;

class PaymentsController extends Controller
{

    private $trackingRepository;
    private $finnotechService;
    private $userRepository;
    private $transactionsRepository;

    public function __construct(ITrackingRepository $trackingRepository, IFinnotechService $finnotechService, IUserRepository $userRepository, ITransferTransactionsRepository $transactionsRepository)
    {
        $this->trackingRepository = $trackingRepository;
        $this->finnotechService = $finnotechService;
        $this->userRepository = $userRepository;
        $this->transactionsRepository = $transactionsRepository;
    }

    /**
     * @OA\Get(
     *    path="/api/v1/pays",
     *    operationId="pays.index",
     *    tags={"Payment"},
     *    description="Get transactions list from finnotech",
     *    @OA\Response(response="200", description="success")
     * )
     */
    public function transactionList()
    {
        try {
            $deposit = 'IR540160000000000835467529';
            $fromDate = '1390/01/01';
            $toDate = '1401/01/01';
            $userId = 1;
            $offset = 0;
            $length = 20;
            $tracking = $this->trackingRepository->store($userId, TrackingTypes::$TransactionList);// TODO :: Generate trackID
            $result = $this->finnotechService->pays($deposit, $tracking->id, $fromDate, $toDate, $offset, $length);// TODO :: call customerInfo from finnotech
            if ($result['ok'] == true) {
                $this->trackingRepository->changeStatus($tracking->id, TrackingStatus::$SUCCESS); // TODO :: change tracking status
                return responseBody(ResponseMessage::$SUCCESS, ResponseStatus::$SUCCESS)->successfully($result['result']); // ret
            }
            $this->trackingRepository->changeStatus($tracking->id, TrackingStatus::$FAIL);
            return responseBody(ResponseMessage::$FAIL, $result['status'])->successfully($result['result']); // TODO :: response body custom helpers method
        } catch (\Exception $e) {
            return responseBody(ResponseMessage::$EXCEPTION, ResponseStatus::$SERVER_ERROR)->faile();
        }
    }

    /**
     * @OA\Get(
     *    path="/api/v1/userInfo",
     *    operationId="pays.userInfo",
     *    tags={"Payment"},
     *    description="Get user info from finnotech",
     *    @OA\Response(response="200", description="success")
     * )
     */
    public function getUserInfo()
    {
        try {
            //TODO :: mock nationalId, userId
            $nationalId = '4270465824';
            $userId = 1;
            $tracking = $this->trackingRepository->store($userId, TrackingTypes::$CustomerInfo);// TODO :: Generate trackID
            $result = $this->finnotechService->customerInfo($nationalId, $tracking->id);// TODO :: call customerInfo from finnotech
            if ($result['ok'] == true) {
                $this->userRepository->update($result['result'], $userId);// TODO :: Update user info
                $this->trackingRepository->changeStatus($tracking->id, TrackingStatus::$SUCCESS); // TODO :: change tracking status
                return responseBody(ResponseMessage::$SUCCESS, ResponseStatus::$SUCCESS)->successfully($result['result']); // ret
            }
            $this->trackingRepository->changeStatus($tracking->id, TrackingStatus::$FAIL);
            return responseBody(ResponseMessage::$FAIL, $result['status'])->successfully($result['result']); // TODO :: response body custom helpers method
        } catch (\Exception $e) {
            return responseBody(ResponseMessage::$EXCEPTION, ResponseStatus::$SERVER_ERROR)->faile();
        }
    }

    /**
     * @OA\Get(
     *    path="/api/v1/transferTo",
     *    operationId="pays.transferTo",
     *    tags={"Payment"},
     *    description="transfer amount to another account",
     *    @OA\Response(response="200", description="success")
     * )
     */
    public function transferTo()
    {
        try {
            $userId = 1;
            $tracking = $this->trackingRepository->store($userId, TrackingTypes::$TransactionList);// TODO :: Generate trackID
            $transferTo = $this->getTransferToInfos();
            $result = $this->finnotechService->transferTo($transferTo, $tracking->id);// TODO :: call customerInfo from finnotech
            if ($result['ok'] == true) {
                $this->transactionsRepository->store($result['result']); // TODO :: store transactions users
                $this->trackingRepository->changeStatus($tracking->id, TrackingStatus::$SUCCESS); // TODO :: change tracking status
                return responseBody(ResponseMessage::$SUCCESS, ResponseStatus::$SUCCESS)->successfully($result['result']); // ret
            }
            $this->trackingRepository->changeStatus($tracking->id, TrackingStatus::$FAIL);
            return responseBody(ResponseMessage::$FAIL, $result['status'])->successfully($result['result']); // TODO :: response body custom helpers method
        } catch (\Exception $e) {
            return responseBody(ResponseMessage::$EXCEPTION, ResponseStatus::$SERVER_ERROR)->faile();
        }
    }

    private function getTransferToInfos(): TransferToInfos
    {
        $transferTo = new TransferToInfos();
        $transferTo->amount = 1;
        $transferTo->description = "شرح تراکنش";
        $transferTo->destinationFirstname = "خلیلی  حسینی  بیابانی";
        $transferTo->destinationLastname = "سمیه   غز اله  فریماه";
        $transferTo->destinationNumber = "IR120620000000302876732005";
        $transferTo->paymentNumber = "123456";
        $transferTo->deposit = "776700000";
        $transferTo->sourceFirstName = "مارتین";
        $transferTo->sourceLastName = "اسکورسیزی";
        $transferTo->reasonDescription = "1";
        return $transferTo;
    }
}
