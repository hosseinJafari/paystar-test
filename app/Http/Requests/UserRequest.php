<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'name' => 'required|min:2|max:255',
                    'mobile' => 'required|unique:users,mobile|regex:/09[0-9]{9}$/',
                    'userType' => 'required|in:user,customer',
                ];
            case 'PATCH':
                return [
                    'name' => 'required|min:2|max:255',
                    'mobile' => 'required|unique:users,mobile,' . $this->id . '|regex:/09[0-9]{9}$/',
                    'userType' => 'required|in:user,customer',
                ];
            default:
                return [
                    'name' => 'required|min:2|max:255',
                    'mobile' => 'required|unique:users,mobile|regex:/09[0-9]{9}$/',
                    'userType' => 'required|in:user,customer',
                ];
        }
    }

    public function messages()
    {
        return [
            'name.required' => ' وارد کردن فیلد نام الزامی است',
            'name.min' => 'فیلد نام نمیتواند کمتر از 2 حرف باشد',
            'name.max' => 'فیلد نام نمیتواند بیشتر از 255 حرف باشد',
            'mobile.required' => 'وارد کردن شماره تماس الزامی است',
            'mobile.regex' => 'فرمت شماره تماس صحیح نمی باشد',
            'mobile.unique' => 'شماره تماس قبلا ثبت شده است',
            'userType.required' => 'وارد کردن نوع کاربر الزامی است',
            'userType.in' => 'نوع کاربر وارد شده معتبر نمی باشد',
        ];
    }
}
