<?php

/**
 * Created by PhpStorm.
 * User: Hossein
 * Date: 7/5/2018
 * Time: 4:02 PM
 */

namespace App\Helpers\Response;

class ResponseBody implements ResponseBodyInterface
{
    protected $status;
    protected $ok;
    protected $message;

    public static function responseBody(string $message, int $status = 200)
    {
        // TODO: Implement responseBody() method.
        $response = new static();
        $response->status = $status;
        $response->message = $message;
        return $response;
    }

    public function successfully($data)
    {
        // TODO: Implement successfuly() method.
        $response = (object)[
            'ok' => true,
            'msg' => $this->message,
            'data' => $data
        ];
        return response()->json($response, $this->status);
    }

    public function fail($data = null)
    {
        // TODO: Implement faile() method.
        $response = new \stdClass();
        $response->ok = false;
        $response->msg = $this->message;
        if ($data == null) {
            $response1 = (object)[
                'ok' => false,
                'msg' => $this->message
            ];
        } else {
            $response->data = $data;
            $response1 = (object)[
                'ok' => false,
                'msg' => $this->message,
                'data' => $data
            ];
        }

        return response()->json($response, $this->status);
    }

}
