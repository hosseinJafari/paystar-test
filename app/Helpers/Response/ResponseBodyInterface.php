<?php
/**
 * Created by PhpStorm.
 * User: Hossein
 * Date: 7/5/2018
 * Time: 4:04 PM
 */

namespace App\Helpers\Response;

interface ResponseBodyInterface
{
    public static function responseBody(string $message, int $status);

    public function successfully($data);

    public function fail();
}

