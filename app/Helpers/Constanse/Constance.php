<?php
/**
 * Created by PhpStorm.
 * User: Hossein
 * Date: 10/24/2018
 * Time: 3:02 PM
 */

namespace App\Helpers\Constanse;


class Constance
{
    public static $STATUS = [
        'SUCCESS' => 200,
        'CREATED_ITEM' => 201,
        'ACCEPTED' => 202,
        'BAD_REQUEST' => 400,
        'UNAUTHORIZED' => 401,
        'FORBIDDEN' => 403,
        'NOT_FOUND' => 404,
        'METHOD_NOT_ALLOWED' => 405,
        'NO_ACCEPTED' => 406,
        'TIME_OUT' => 408,
        'CONFLICT' => 409,
        'GONE' => 410,
        'UNPROCESSABLE_ENTITY' => 422,
        'SERVER_ERROR' => 500,
        'NOT_IMPLEMENTED' => 501,
        'BAD_GETWAY' => 502,
        'SERVICE_UNAVILABLE' => 503,
        'GETWAY_TIMEOUTE' => 504,
    ];

    public static $MESSAGES = [
        'LIST_INDEX' => 'List of all items.',
        'CREATED_SUCCESS' => 'Item created.',
        'CREATED_FAIL' => 'Item not created',
        'EDIT_DETAILS' => 'Item detail',
        'SHOW_DETAILS' => 'Item detail',
        'NOT_FOUND' => 'Item not found',
        'UPDATE_SUCCESS' => 'Item is updated',
        'UPDATE_FAIL' => 'Item not updated',
        'DELETE_SUCCESS' => 'Item deleted',
        'DELETE_FAIL' => 'Item can not delete',
        'FORBIDDEN' => 'Forbidden',
    ];
}