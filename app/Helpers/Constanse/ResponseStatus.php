<?php


namespace App\Helpers\Constanse;


class ResponseStatus
{
    public static $SUCCESS = 200;
    public static $CREATED_ITEM = 201;
    public static $ACCEPTED = 202;
    public static $BAD_REQUEST = 400;
    public static $UNAUTHORIZED = 401;
    public static $FORBIDDEN = 403;
    public static $NOT_FOUND = 404;
    public static $METHOD_NOT_ALLOWED = 405;
    public static $NO_ACCEPTED = 406;
    public static $TIME_OUT = 408;
    public static $CONFLICT = 409;
    public static $GONE = 410;
    public static $UNPROCESSABLE_ENTITY = 422;
    public static $SERVER_ERROR = 500;
    public static $NOT_IMPLEMENTED = 501;
    public static $BAD_GETWAY = 502;
    public static $SERVICE_UNAVILABLE = 503;
    public static $GETWAY_TIMEOUTE = 504;
}
