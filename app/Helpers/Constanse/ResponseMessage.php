<?php

namespace App\Helpers\Constanse;

class ResponseMessage
{
    public static $SUCCESS = 'Successfully';
    public static $FAIL = 'Fail';
    public static $LIST_INDEX = 'List of all items.';
    public static $CREATED_SUCCESS = 'Item created.';
    public static $CREATED_FAIL = 'Item not created';
    public static $EDIT_DETAILS = 'Item detail';
    public static $SHOW_DETAILS = 'Item detail';
    public static $NOT_FOUND = 'Item not found';
    public static $UPDATE_SUCCESS = 'Item is updated';
    public static $UPDATE_FAIL = 'Item not updated';
    public static $DELETE_SUCCESS = 'Item deleted';
    public static $DELETE_FAIL = 'Item can not delete';
    public static $FORBIDDEN = 'Forbidden';
    public static $EXCEPTION = 'Exception Error';
}
