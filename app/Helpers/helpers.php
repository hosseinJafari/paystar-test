<?php

use App\AesCipher;
use App\Helpers\Validation\ValidateType;
use Carbon\Carbon;
use App\Models\VerificationCode;
use App\Helpers\String\StrFloat;
use App\Helpers\String\StrRandom;

/**
 *  TODO : Set message session
 */
function flash($title = NULL, $message = NULL)
{
    $flash = app('App\Helpers\SweetAlert\Flash');

    if (func_num_args() == 0) {
        return $flash;
    }

    return $flash->info($title, $message);
}

/**
 *  TODO : Convert numbers to persian
 */
function convert($string, $state = true)
{
    $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    return $state ? str_replace($english, $persian, $string) : str_replace($persian, $english, $string);
}

/**
 *  TODO : Convert numbers to arrabic and revers
 */
function convert2Arabic($str, $status = false)
{
    $western_arabic = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    $eastern_arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');

    return $status ? str_replace($eastern_arabic, $western_arabic, $str) : str_replace($western_arabic, $eastern_arabic, $str);
}

/**
 *  TODO : Get User With auth api token
 */
function getUserAuthApi()
{
    return Auth::guard('api')->user();
}

/**
 *  TODO : Add response body helper
 */
function responseBody($message = NULL, $status = 200)
{
    $response = app('App\Helpers\Response\ResponseBody');
    //    if (func_num_args() == 0) {
    //        return $response;
    //    }

    return $response->responseBody($message, $status);
}

/**
 *  TODO : helper for status constanse
 */
function status(string $status): int
{
    return \App\Helpers\Constanse\Constance::$STATUS[$status];
}

/**
 *  TODO : helper for message constanse
 */
function message(string $title): string
{
    // TODO : Return
    return \App\Helpers\Constanse\Constance::$MESSAGES[$title];
}

/**
 *  TODO : Get color part
 */
function random_color_part()
{
    return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
}

/**
 *  TODO : Get random color
 */
function random_color()
{
    return '#' . random_color_part() . random_color_part() . random_color_part();
}



