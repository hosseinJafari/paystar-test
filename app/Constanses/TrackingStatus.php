<?php


namespace App\Constanses;


class TrackingStatus
{
    public static $INIT = 'init';
    public static $SUCCESS = 'success';
    public static $FAIL = 'fail';
}
