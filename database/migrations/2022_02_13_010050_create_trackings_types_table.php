<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackingsTypesTable extends Migration
{
    public function up()
    {
        Schema::create('trackings_types', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->timestamps();
        });

        \Illuminate\Support\Facades\DB::table('trackings_types')->insert([
            ['title' => 'دریافت اطلاعات مشتری'],
            ['title' => 'انتقال وجه بین حساب'],
            ['title' => 'دریافت لیست تراکنش ها']
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('trackings_types');
    }
}
