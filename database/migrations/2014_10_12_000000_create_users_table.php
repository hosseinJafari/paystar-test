<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('firstName')->nullable();
            $table->string('lastName')->nullable();
            $table->string('birthdate')->nullable();
            $table->string('mobile')->unique();
            $table->string('nationalId')->unique();
            $table->string('customerTypeCode')->nullable();
            $table->string('customerDescription')->nullable();
            $table->string('password')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        \Illuminate\Support\Facades\DB::table('users')->insert([
            [
                'firstName' => 'hossein',
                'lastName' => 'jafari',
                'birthdate' => '1370/09/23',
                'mobile' => '09191416876',
                'nationalId' => '4270465824',
            ]
        ]);
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
