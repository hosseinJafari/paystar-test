<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackingsTable extends Migration
{
    public function up()
    {
        Schema::create('trackings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('trackingTypeId')->index();
            $table->unsignedBigInteger('userId')->index();
            $table->enum('status',['init','success','fail'])->default('init');
            $table->timestamps();
            $table->foreign('trackingTypeId')->on('trackings_types')->references('id')->onDelete('cascade');
            $table->foreign('userId')->on('users')->references('id')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('trackings');
    }
}
